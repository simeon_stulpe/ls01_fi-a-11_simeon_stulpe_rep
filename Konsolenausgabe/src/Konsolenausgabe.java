
public class Konsolenausgabe {

	public static void main(String[] args) {
		String s = "Java-Programm";
		// Standardausgabe
		System.out.printf( "\n|%s|\n", s ); // |Java-Programm|
		
		// rechtsbündig mit 20 Stellen
		System.out.printf( "|%20s|\n", s ); // | Java-Programm|
		
		// linksbündig mit 20 Stellen
		System.out.printf( "|%-20s|\n", s ); // |Java-Programm |
		
		// minimal 5 stellen
		System.out.printf( "|%5s|\n", s ); // |Java-Programm|
		
		// maximal 4 Stellen
		System.out.printf( "|%.4s|\n", s ); // |Java|
		
		// 20 Positionen, rechtsbündig, höchstens 4 Stellen von String
		System.out.printf( "|%20.4s|\n", s ); // | Java|
		
		
	}
	
}
